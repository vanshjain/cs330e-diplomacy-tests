#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i, "A")
        self.assertEqual(j, "Madrid")
        self.assertEqual(k, "Hold")

    def test_read_1(self):
        s = "B Barcelona Move Madrid\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, "B")
        self.assertEqual(j, "Barcelona")
        self.assertEqual(k, "Move")
        self.assertEqual(l, "Madrid")

    def test_read_1(self):
        s = "D Paris Support B\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, "D")
        self.assertEqual(j, "Paris")
        self.assertEqual(k, "Support")
        self.assertEqual(l, "B")


    # ----
    # eval
    # ----

    # -----
    # print
    # -----

    # def test_print(self):
    #     w = StringIO()
    #     diplomacy_print(w, 1, 10, 20)
    #     self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----


    def test_solve_1(self):
        r = StringIO("A Paris Move London\nB Barcelona Move Madrid\nC Austin Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Madrid\nC Austin\n")

    def test_solve_2(self):
        r = StringIO("A Paris Move London\nB London Move Paris\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Paris\n")

    def test_solve_3(self):
        r = StringIO("A London Hold\nB Paris Hold\nC Rome Support A\nD Madrid Move Paris\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB [dead]\nC Rome\nD [dead]\n")
            
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF NewYork Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE [dead]\nF [dead]\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
