from unittest import main, TestCase
from Diplomacy import diplomacy_solve
from io import StringIO

class TestDiplomacy(TestCase):

    def test_solve1(self):

        r = StringIO("A Madrid Support C\nB Paris Support C\nC Tokyo Move Atlanta\nD Atlanta Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Paris\nC Atlanta\nD [dead]\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve3(self):
        r = StringIO("A Austin Support C\nB Madrid Hold\nC Paris Move Austin\nD Atlanta Move Paris")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

if __name__ == "__main__": #pragma: no cover
    main()
